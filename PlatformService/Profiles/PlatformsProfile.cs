﻿using System;
using AutoMapper;
using PlatformService.DTOs;
using PlatformService.Models;

namespace PlatformService.Profiles
{
    public class PlatformsProfile : Profile
    {
        public PlatformsProfile()
        {
            // Source -> Target
            CreateMap<Platform, PlatformReadDTO>();
            CreateMap<PlatformCreateDTO, Platform>();
            CreateMap<PlatformReadDTO, PlatformPublishedDto>();

            CreateMap<Platform, GrpcPlatformModel>()
                .ForMember(dest => dest.PlatformId, opts => opts.MapFrom(source => source.Id))
                .ForMember(dest => dest.Name, opts => opts.MapFrom(source => source.Name))
                .ForMember(dest => dest.Publisher, opts => opts.MapFrom(source => source.Publisher));
        }
    }
}

